
<!-- README.md is generated from README.Rmd. Please edit that file -->

# cleanmdo

<!-- badges: start -->
<!-- badges: end -->

The goal of cleanmdo is to clean MDO files.

## Installation

You can install the development version of this package on `gitlab`
using the `remotes` R package:

``` r
remotes::install_gitlab("dickoa/cleanmdo")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(cleanmdo)
path <- "niger_mdo_week50.xlsx"
clean_ner(path)
```
