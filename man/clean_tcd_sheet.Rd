% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cleanmdo.R
\name{clean_tcd_sheet}
\alias{clean_tcd_sheet}
\title{Clean a single sheet from the Tchad MDO data}
\usage{
clean_tcd_sheet(file, sheet, skip = 5)
}
\arguments{
\item{file}{character, the Excel file.}

\item{sheet}{character, the name of the sheet.}

\item{skip}{integer, the number of lines to skip.}
}
\value{
A tibble
}
\description{
Clean a single sheet from the Tchad MDO data
}
